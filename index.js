const { resolve } = require('path');
require('dotenv').config({ path: resolve(__dirname, '.env') });
const stripe = require('stripe')(process.env.STRIPE_SECRET);
const express = require('express');
const app = express();
const bodyParser = require('body-parser');

// Use JSON parser for all non-webhook routes.
app.use((req, res, next) => {
  if (req.originalUrl === '/webhook') {
    next();
  } else {
    bodyParser.json()(req, res, next);
  }
});

app.get('/', (_req, res) => res.json({ ok: true }));

// Stripe requires the raw body to construct the event.
app.post('/webhook', bodyParser.raw({ type: 'application/json' }), (req, res) => {
  let event;

  try {
    event = stripe.webhooks.constructEvent(req.body, req.headers['stripe-signature'], process.env.STRIPE_WEBHOOK_SECRET);
  } catch (error) {
    // On error, log and return the error message
    console.log(`❌ Error message: ${error.message}`);
    return res.status(400).send(`Webhook Error: ${error.message}`);
  }

  console.log(`✨ Event: ${event.type}`);
  console.log(JSON.stringify(event, null, 2) + '\n\n');

  // Return a response to acknowledge receipt of the event
  res.json({ received: true });
});

app.post('/create-customer', async (req, res) => {
  const { email } = req.body;
  try {
    // Create a new customer object
    const customer = await stripe.customers.create({
      email,
    });

    // Create a CheckoutSession to set up our payment methods recurring usage
    const checkoutSession = await stripe.checkout.sessions.create({
      payment_method_types: ['card'],
      mode: 'setup',
      customer: customer.id,
      success_url: `http://localhost:3000/checkout-session/{CHECKOUT_SESSION_ID}`,
      cancel_url: `http://localhost:3000/`,
      shipping_address_collection: {
        allowed_countries: ['RU', 'US']
      }
    });

    res.send({ customer, checkoutSession });
  } catch (error) {
    res.status(400).send({ error });
  }
});

app.get('/checkout-session/:id', async (req, res) => {
  const { id } = req.params;

  const checkoutSession = await stripe.checkout.sessions.retrieve(id, {
    expand: ['customer', 'setup_intent.payment_method'],
  });

  res.send({ checkoutSession });
});

app.listen(3000, () => console.log(`Node server listening on port 3000!`));
